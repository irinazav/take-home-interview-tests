----------------------------------------------------

# **Take-home interview tests (mini-projects)**

----------------------------------------------------


## **Test descriptions with  links:**


## **1)  Contact management**    

code source & screenshots:    
**[bitbucket.org/izav/project-test-signal-r/src](https://bitbucket.org/izav/project-test-signal-r/src)**  


software used:     
C#, ASP.NET MVC, EF (Code First), MS SQL Server, jQuery, Templates, Ajax,  Bootstrap, Hangfire, SignalR

**Test:**

Build a very simple contact management MVC application using C#.

** Applications:**

* Visual Studio 2013 or 2015 (C# .net 4.5 or lower)
* SQL Server 2014 (or lower)

**Contact fields:**

* Name (FirstName, LastName)
* Phone
* Email
* Address (Street, City, State, Postal Code)

**Objectives:**

* Use Entity Framework to interface with DB
* Search, list Contact records
* Allow user to Add, edit, delete Contact records. For Edit Use AJAX (no full posts) from a View dedicated to Contact Edit
* Use a SQL Trigger to log changes and deletes to Contact records
* SQL Procedure (via Entity Framework method) to return number of changes and last updated date/time for the current Contact Record. Display on the Contact Edit window.

**Bonus points for any of the following (not required):**

* Use any third-party API (Google Maps, USPS, etc.) to validate/verify the address
* Implement Hangfire.io. Create a new field DisplayName. Schedule a job that runs every 30 seconds that updates DisplayName (from FirstName and LastName) for any new Contact records.
* Implement SignalR. Synchronize Contact record edits across all open browser windows.

----------------------------------------------------

## **2)  Tiny URL** 
code source & screenshots:     
**[bitbucket.org/izav/tiny-url-test/src](https://bitbucket.org/izav/tiny-url-test/src)**   


software used:     
C#, MVC, EF(Code First), MS SQL Server, jQuery, Templates, Ajax,  Bootstrap

**Exercise:**

Design and implement a tinyurl (shortened URL) application.  The application should:

*  Accept as input a regular URL and return a tinyurl associated with the input URL.
* Accept as input a previously generated tinyurl and return the full URL associated with it

You can use any algorithm desired for generating the tinyurl and it need not be universally unique. Please do not use an existing public API.

**Constraints:**

* Application must be coded in C#, MVC
* Application should be complete front to back end solution including DB.
* Application needs to be browser based
* Solution file and source code should be self-contained such that we can compile and run the application here.


-----------------------------------------------


## **3)  Messages Database & Golden Pond**
code source & screenshots:   
**[bitbucket.org/izav/test-messagesdb-and-goldenpond/src](https://bitbucket.org/izav/test-messagesdb-and-goldenpond/src)**  

software used:     
C#, ASP.NET MVC, EF(Code First), MS SQL Server, Bootstrap 

**a) Messages Database**
 

![test.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/test.jpg)

Given the above database tables (with some sample data) write a SQL query that…

* lists of each domain in the systemL.
* lists of each email address along with the owner’s real name
* lists each domain and the total number of emails sent to it (combining To, CC, & BCC)

**b) Golden Pond**    

You are writing a simulation of ducks on a curiously rectangular pond. A duck’s position and location is represented by a combination of x and y co-ordinates and a letter representing one of the four cardinal compass points. 
The pond is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the duck is in the bottom left corner and facing North.  
In order to control a duck, you send a simple string of letters. The possible letters are 'P', 'S' and 'F'. 'P' and 'S' makes the duck spin 90 degrees toward port side (left) or starboard (right) respectively, without moving from its current spot. 'F' means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

* Test Input:   
1 2 N  PFPFPFPFF    
3 3 E  FFSFFSFSSF  
* Expected Output:   
1 3 N  
5 1 E  

----------------------------------------------------

## **4)  GetSwift Code Test** 
code source & screenshots:   
**[bitbucket.org/izav/getswift-test/src](https://bitbucket.org/izav/getswift-test/src)**  


software used:     
C#, ASP.NET MVC, Linq, Bootstrap 

**The scenario**

You run a drone delivery company. Packages arrive at your depo to be delivered by your fleet of drones.

**The problem**

Your solution should take two inputs: a list of drones, and a list of packages; and produce two outputs: a list of assignments of packages to drones, and a list of packages that could not be assigned. [Read more...](https://github.com/GetSwift/codetest)

```javascript
{
  assignments: [{droneId: 1593, packageId: 1029438}, {droneId: 1251, packageId: 1029439}]
  unassignedPackageIds: [109533, 109350, 109353]
}
```

**Constraints**

- The depo is located at 303 Collins Street, Melbourne, VIC 3000
- Drones might already be carrying a package. The time to deliver this package should be taken into account when comparing drones.
- Once a drone is assigned a package, it will fly in a straight line to its current destination (if it already has a package), then to the depo, then to the new destination
- Packages must only be assigned to a drone that can complete the delivery by the package's delivery deadline
- Packages should be assigned to the drone that can deliver it soonest
- A drone should only appear in the assignment list at most once

**Assumptions**

- Drones have unlimited range
- Drones travel at a fixed speed of 50km/h
- Packages are all the same weight and volume
- Packages can be delivered early
- Drones can only carry one item at a time


**The API**

You should integrate with [this API](https://codetest.kube.getswift.co/drones) which generates randomized data.  
You'll need to use two methods:

**GET /drones **

This returns  a randomized list of drones. A drone can have up to one package assigned to it.

```javascript
[
    {
        "droneId": 321361,
        "location": {
            "latitude": -37.78290448241537,
            "longitude": 144.85335277520906
        },
        "packages": [
            {
                "destination": {
                    "latitude": -37.78389758422243,
                    "longitude": 144.8574574322506
                },
                "packageId": 7645,
                "deadline": 1500422916
            }
        ]
    },
    {
        "droneId": 493959,
        "location": {
            "latitude": -37.77718638788778,
            "longitude": 144.8603578487479
        },
        "packages": []
    }
]
```

**GET /packages** 

This returns  a randomized list of packages. You will need to assign the packages from this endpoint to the drones returned from the other endpoint. The deadline here is a Unix timestamp.

```javascript
[
    {
        "destination": {
            "latitude": -37.78404125474984,
            "longitude": 144.85238118232522
        },
        "packageId": 8041,
        "deadline": 1500425202
    },
    {
        "destination": {
            "latitude": -37.77058198385452,
            "longitude": 144.85157121265505
        },
        "packageId": 8218,
        "deadline": 1500423287
    }
]
```




----------------------------------------------------

## **5) Pyramid data** 

code source & screenshots:   
**[bitbucket.org/izav/test-pyramid/src](https://bitbucket.org/izav/test-pyramid/src)**  

software used:     
C#, Linq

Javascript / C#:  Use either Javascript or C# to write a code snippet to address the following problem:

**Problem 1**

Assume an array (javascript) or list (C#) of objects in pyramid structure with the following properties: id , name, value, parentid

Example (C#):


```
#!c#

 var b = new block();
 b.id = 100; 
 b.name = "block 100";
 b.value = 102.50;
 b.parentid = 99;
```

 
Write a recursive function that accepts an ID as the only parameter and will loop through an array or list of an undetermined size and number of levels. The recursive function will calculate the sum of the value of all the blocks that are in the hierarchy under the ID passed to the function.

**Problem 2** 

With the same scenario as in problem 1, write a non-recursive routine that also accepts an ID, and returns a list of id's of all the blocks that are in the hierarchy under the ID passed to the function.

------------------------------------------------------------

## **6) Finding Popular Build-Your-Own Pizzas**
code source & screenshots:   
**[bitbucket.org/izav/test-olo/src](https://bitbucket.org/izav/test-olo/src)** 

software used:     
C#, Linq


**Test:**    
A pizza chain wants to know which topping combinations
are most popular for Build Your Own pizzas.

Given the sample of orders at http://files.olo.com/pizzas.json,



```
#!javascript

data = [{
   "toppings": [ "bacon", "four cheese"]
  },
  {
   "toppings": [ "bacon" ]
  },
```

write an application (in C# or F#) to output the top 20 most 
frequently ordered pizza configurations, listing the toppings 
for each along with the number of times that pizza configuration 
has been ordered.

---------------------------------------------------------------

## **7) Display the student's name with the highest average**
code source & screenshots:   
**[bitbucket.org/izav/linqpad-test/src](https://bitbucket.org/izav/linqpad-test/src)**

software used:     
C#, Linq, Linqpad


**Test:**    
Download [this CSV](http://www.courtalert.com/jobs/Interview.zip) with a row id, student name, subject and score.  
Calculate the average for each student and display the student's name with the highest average. Write the code in Linqpad (http://www.linqpad.net/). 
 
Included is a .linq file you can use as a starting point.



```
#!c#



void Main()

{ 
 
	const string fileName = @"c:\temp\scores.csv";

	String[] lines = File.ReadAllLines(fileName);

	//output the lines

	lines.Dump();
	
	//todo calculate the average for each student and display the student�s name with the highest average.

}

```





